#/bin/bash

file="name.txt" #"/Users/anglil/Downloads/crawler/name.txt"
count=0
while IFS= read -r line
do
	extension='.txt'
	filename="/home/anglil/facecrawl/$line$extension" #"$line$extension" #"/Users/anglil/Downloads/crawler/$line$extension"
	insideCount=0
	## check if the directory already exists
	if [ ! -d "$line" ]; 
	then
		mkdir $line
	fi
	## enter the directory
	cd $line
	while IFS= read -r link
	do 
		#echo $link
		#curl -O $link

		imgExtension="${link##*.}"
		imgExtension="${imgExtension%%[^a-zA-Z]*}"
		echo "extension:"
		echo $imgExtension
		
		imgName="${link##*/}"
		echo "original name:"
		echo $imgName
		
		imgNameNew="$insideCount.$imgExtension"
		echo "new name:"
		echo $imgNameNew
		
		#if [ -f "$imgName" ]
		#then
		#	mv $imgName $imgNameNew

		curl -o $imgNameNew $link		
		echo '\n'		

		let count=$count+1
		let insideCount=$insideCount+1
	done <"$filename"
	cd ..
done <"$file"
